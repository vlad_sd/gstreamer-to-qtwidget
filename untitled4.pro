#-------------------------------------------------
#
# Project created by QtCreator 2017-02-01T19:26:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = untitled4
TEMPLATE = app

GstreamerDir=$$(GSTREAMER_1_0_ROOT_X86)

INCLUDEPATH = $${GstreamerDir}/include/gstreamer-1.0
INCLUDEPATH += $${GstreamerDir}/include/glib-2.0
INCLUDEPATH += $${GstreamerDir}/lib/glib-2.0/include
INCLUDEPATH += $${GstreamerDir}/lib/gstreamer-1.0/include

LIBS = $${GstreamerDir}/lib/gstreamer-1.0.lib
LIBS += $${GstreamerDir}/lib/*.lib
SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
